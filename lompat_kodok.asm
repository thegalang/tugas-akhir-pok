.include "m8515def.inc"
;keypad read input section
;code referenced from http://www.avr-asm-tutorial.net/avr_en/apps/key_matrix/keypad/keyboard.html#io
.def temp = R16 ; define a multipurpose register
.def temp2 = R17
.def col = R20
.def line = R21
.def PB = r24 ; for PORTB

; -- function arguments
.def arg1  = r2
.def arg2 = r3
.def arg3 = r4

; -- function return values
.def ret1 = r5
.def ret2 = r6
.def ret3 = r7

.def empty_rock = r8 ; -- register dengan batu kosong
.def s0 = r9 ; -- register save multifungsi
.def s1 = r10 ; -- register save multifungsi
.def s2 = r11 ; -- register save multifungsi

; -- timer variables
.def seconds_passed = r23
.def next_led = r12
.def led_lights = r13

; -- constants
.equ KODOK_ARR = 0x60
.equ twenty_seconds = 110


rjmp enableinterrupt

.org $06
	rjmp ov_int ; overflow interrupt

.org $0D
rjmp ext_int2 ;use external interrupt 2


; -- setting interrupt
enableinterrupt:
	; -- button interrupt
	ldi r16, $00
	out PORTE, r16
	ldi r17,0b00001010 	
	out MCUCR,r17		; activate interrupt saat falling edge
	ldi r17,0b00100000	; enable external interrupt request 2 (bit 5 set jadi 1)
	out GICR,r17		; 

	; -- timer interrupt
	ldi temp, (1<<CS10)
	out TCCR1B,temp			
	ldi temp,1<<TOV1
	out TIFR,temp		; set timer interrupt
	ldi temp,1<<TOIE1
	out TIMSK,temp		; enable timer overflow flag
	ser temp
	out DDRB,temp		; buat pin port B jadi output semua
	sei					; instruksi untuk activate interrupt

INIT_STACK:
	ldi temp, low(RAMEND)
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp
	ldi XH, high(KODOK_ARR)
	ldi XL, low(KODOK_ARR)
	ldi temp, 1 ; selalu cetak bawah, kecuali diminta atas
	mov arg3, temp
	rcall INIT_GAME

	
init_keypad:
    ldi temp, 0b11110000 ; data direction register column lines output
    out DDRD, temp    ; set direction register
    ldi temp, 0b00001111 ; Pull-Up-Resistors to lower four port pins
    out PORTD, temp    ; to output port
    rjmp read_key

; --- BAGIAN CEK KONDISI AKHIR GAME ---

; -- permainan sudah dimenangkan
WIN_GAME:
		
	ldi ZL, low(2*String_menang)
	ldi ZH, high(2*String_menang)
	rjmp END_GAME

; -- pemain kalah (waktu habis)
LOSE_GAME:
	
	ldi ZL, low(2*String_kalah)
	ldi ZH, high(2*String_kalah)

; -- asumsi: ZH:ZL sudah berisi benar (SUCCESS/FAILURE)
END_GAME:

	ldi temp, 0
	mov arg3, temp ;-- setting nyetak jadi diatas

	rcall WRITE_WORD

	; -- balikin nyetak dibawah, barangkali reset kan
	ldi temp, 1
	mov arg3, temp

	; -- biar timer ga jalan lagi
	ldi seconds_passed, 0
	
	; -- infinite loop, sampai button interrupt
	rjmp loop_end_game

; --- infinite loop sampai tombol reset ditekan
loop_end_game:
	rjmp loop_end_game

; --- BAGIAN CEK KONDISI AKHIR GAME SELESAI ---

; --- BAGIAN KEYPAD ---
read_key:
		
		; cek apakah target sudah tercapai?
		rcall CEK_FINISH
		ldi temp, 1
		cp ret1, temp
		breq WIN_GAME

		;init untuk mengecek input keypad
    ldi col, 4
    ldi line, 1

    ldi temp, 0b00001111 ; PB4..PB6=Null, pull-Up-resistors to input lines
    out PORTD, temp    ; of port pins PB0..PB3
    in temp, PIND    ; read key results
    ori temp,0b11110000 ; mask all upper bits with a one
    cpi temp,0b11111111 ; all bits = One?
 	
    breq read_key        ; yes, no key is pressed
    rjmp ReadKey

ReadKey:
    ldi ZH,HIGH(2*KeyTable) ; Z is pointer to key code table
    ldi ZL,LOW(2*KeyTable)
 		
    ; read column 4
    ldi temp, 0b01111111 ; PB7 = 0
    out PORTD, temp
    in temp, PIND ; read input line
    ori temp, 0b11110000 ; mask upper bits
    cpi temp, 0b11111111 ; a key in this column pressed?
    brne KeyRowFound ; key found
    adiw ZL,4 ; column not found, point Z one row down
    subi col, 1 ;decrement column
 
    ; read column 3
    ldi temp, 0b10111111 ; PB6 = 0
    out PORTD, temp
    in temp, PIND ; read input line
    ori temp, 0b11110000 ; mask upper bits
    cpi temp, 0b11111111 ; a key in this column pressed?
    brne KeyRowFound ; key found
    adiw ZL,4 ; column not found, point Z one row down
    subi col, 1 ;decrement column
 
    ; read column 2
    ldi temp, 0b11011111 ; PB5 = 0
    out PORTD, temp
    in temp, PIND ; read again input line
    ori temp, 0b11110000 ; mask upper bits
    cpi temp, 0b11111111 ; a key in this column?
    brne KeyRowFound ; column found
    adiw ZL,4 ; column not found, another four keys down
    subi col, 1 ;decrement column
 
    ; read column 1
    ldi temp, 0b11101111 ; PB4 = 0
    out PORTD, temp
    in temp, PIND ; read again input line
    ori temp, 0b11110000 ; mask upper bits
    cpi temp, 0b11111111 ; a key in this column?
    brne KeyRowFound ;column found
 
    ldi col, 4 ;balikin lagi column ke 4
    breq read_key ; unexpected: no key in this column pressed
 
KeyRowFound: ; column identified, now identify row
    lsr temp ; shift a logic 0 in left, bit 0 to carry
    brcc KeyProc ; a zero rolled out, key is found
 
    adiw ZL,1 ; point to next key code of that column
    subi line, -1 ;increment line
    rjmp KeyRowFound ; repeat shift	

;process keyinput
KeyProc:

	lpm
	
	; -- tombol ke-r0 ditekan, cek apakah valid
	ldi temp, 0xFF
	CP r0, temp
	brne valid_key ; -- jika nilanya 0xFF, tombol tidak valid
		rcall ILLEGAL_MOVE
		rjmp read_key

	valid_key:
	mov arg1, r0
	rcall move_kodok

	rcall DELAY_01
  	rjmp read_key

; --- bagian keypad selesai

; --- BAGIAN PENGGERAKKAN KODOK ---

; -- menggerakkan (jika bisa) kodok ke-arg1
move_kodok:
	
	rcall get_index ; -- dapetin tipe kodok

	mov s0, ret1 ; -- s0: tipe kodok
	mov s1, arg1 ; -- s1: posisi awal kodok

	ldi temp2, 0x41
	cp s0, temp2 ; -- apakah tipe kodok == A?
	brne continue_move_kodok1
		; -- kodok tipe A dihandle disini

		; -- apakah batu kosongnya dibelakang?
		cp empty_rock, s1
		brlt continue_move_kodok2

		; -- apakah selisih <= 2?
		mov temp, empty_rock
		sub temp, s1
		ldi temp2, 3
		cp temp, temp2 ; -- empty_rock - posisi <= 2?
		brge continue_move_kodok2

		; -- legal move, tuker kodok
		mov arg1, s1
		mov arg2, empty_rock
		rcall swap_kodok
		ret

	continue_move_kodok1:
	ldi temp2, 0x42
	cp temp2, s0
	brne continue_move_kodok2
		; -- kodok tipe B dihandle disini

		; -- batu kosongnya didepan?
		cp empty_rock, s1
		brge continue_move_kodok2

		; -- apakah selisih <=2
		mov temp, s1
		sub temp, empty_rock
		ldi temp2, 3 ; -- posisi - empty_rock <= 2
		cp temp, temp2
		brge continue_move_kodok2

		; -- legal move, tuker kodok
		mov arg1, s1
		mov arg2, empty_rock
		rcall swap_kodok
		ret

	continue_move_kodok2:
	
	; -- jika sampai disini belum return, illegal move
	rcall ILLEGAL_MOVE
	ret

; -- memindahkan kodok pada posisi arg1 ke arg2
swap_kodok:
	
	
	rcall get_index ; -- dapetin karakter kodok yang pindah
	mov s0, arg1
	mov s1, arg2
	mov s2, ret1

	; -- hapus kodok pada arg1
	ldi temp, 0x5F
	mov arg1, temp
	mov arg2, s0
	rcall UPDATE_RAM
	rcall WRITE_TEXT

	; -- taruh kodok diatas
	ldi temp, 0
	mov arg3, temp
	mov arg1, s2
	mov arg2, s0
	rcall WRITE_TEXT

	; -- hapus kodok diatas
	ldi temp, 0x20
	mov arg1, temp
	mov arg2, s0
	rcall WRITE_TEXT

	; -- taruh kodok diatas posisi
	mov arg1, s2
	mov arg2, s1
	rcall WRITE_TEXT

	; -- hapus kodok diatas
	ldi temp, 0x20
	mov arg1, temp
	mov arg2, s1
	rcall WRITE_TEXT

	; -- taruh kodok ke arg2
	ldi temp, 1
	mov arg3, temp
	mov arg1, s2
	mov arg2, s1
	rcall UPDATE_RAM
	rcall WRITE_TEXT

	mov empty_rock, s0 ; -- update posisi batu yang kosong
	ret

; -- kedipkan LED jika gerakkan illegal
ILLEGAL_MOVE:
	
	mov temp, led_lights
	ori temp, 0x80
	out PORTC, temp
	rcall DELAY_01

	mov temp, led_lights
	out PORTC, temp
	rcall DELAY_01

	ret

; -- mengecek apakah posisi target sudah dipenuhi --
CEK_FINISH:
	
	ldi ZL, low(Kodok_akhir*2)
	ldi ZH, high(Kodok_akhir*2)
	ldi XH, high(KODOK_ARR)
	ldi XL, low(KODOK_ARR)

	; -- temp dan temp2 dipakai perbandingan
	lpm temp, Z+
	ld temp2, X+

	; baca dari flash dan bandingkan apakah sama dengan target
	loop_cek_finish:

		cp temp, temp2
		
		breq lanjut_cek_finish
			; tidak sama, gagal
			ldi temp, 0
			mov ret1, temp
			ret

		lanjut_cek_finish:
		ld temp2, X+
		lpm temp, Z+

		TST temp
		brne loop_cek_finish ; -- tidak nol, lanjut gan

	ldi temp, 1
	mov ret1, temp
	ret

; ---- BAGIAN PENGGERAKKAN KODOK SELESAI ----

; ---- BAGIAN INPUT/OUTPUT ----

; -- nulis arg1 pada posisi arg2 (arg3, 0: atas, 1: bawah)
WRITE_TEXT:
	
	; naik atau turun, set di temp2
	ldi temp2, 0x40
	mul temp2, arg3
	mov temp2, r0

	; -- set address tulis ke $40 + 2*arg2
	mov temp, arg2
	add temp, temp
	sub temp, temp2
	mov PB, temp
	out PORTB, PB
	sbi PORTB, 7
	sbi PORTA, 0 
	cbi PORTA, 0

	; -- tulis di adress sekarang
	sbi PORTA,1 ; SETB RS
	mov temp, arg1
	out PORTB, temp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	cbi PORTA, 1 ; CLR RS
	rcall DELAY_01
	ret

; -- menulis sebuah kata yang dimulai di ZL:ZH dan diakhiri 0x00
; -- arg3= 0: atas, arg3=1: bawah
WRITE_WORD:

	ldi temp, 0
	mov s0, temp ; -- counter

	lpm temp, Z+
	
	; -- looping setiap karakter satu-satu
	loop_write_word:
		mov arg1, temp
		mov arg2, s0
		rcall WRITE_TEXT

		ldi temp, 1
		add s0, temp ; -- increase counter

		lpm temp, Z+
		TST temp
		brne loop_write_word

	ret

; -- mendapatkan karakter ke-arg1 pada RAM
; -- mengembalikannya ke ret1
get_index:
	ldi XL, low(KODOK_ARR)
	ldi XH, high(KODOK_ARR)
	add XL, arg1
	ld ret1, X
	ret

; -- update index arg2 dengan arg1
UPDATE_RAM:
	ldi XL, low(KODOK_ARR)
	ldi XH, high(KODOK_ARR)
	add XL, arg2
	st X, arg1
	ret
; --- BAGIAN INISIALISASI ---

INIT_GAME:
	rcall INIT_LEDS
	rcall INIT_LCD
	rcall INIT_KODOK
	ret

CLEAR_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,$01 ; MOV DATA,0x01
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

INIT_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,0x38 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	cbi PORTA,1 ; CLR RS
	ldi PB,$0C ; MOV DATA,0x0C --> disp ON, cursor ON, blink OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	rcall CLEAR_LCD ; CLEAR LCD
	cbi PORTA,1 ; CLR RS
	ldi PB,$06 ; MOV DATA,0x06 --> increase cursor, display sroll OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

; -- inisialisasi LED
INIT_LEDS:
	
	ldi temp, 0x01

	; -- buat animasi lampu nyala
	loop_init_leds:
		
		out PORTC, temp
		rcall DELAY_01
		lsl temp

		tst temp
		brne loop_init_leds
		
	ldi temp, 0x00
	mov led_lights, temp
	out PORTC, led_lights
	rcall DELAY_01

	ldi temp, 1
	mov next_led, temp ; -- setting led nyala
	ldi seconds_passed, twenty_seconds ; -- setting counter timer
	ret
	
; set array kodok jadi A A A _ B B B
INIT_KODOK:
	
	ldi ZL, low(Kodok_awal*2)
	ldi ZH, high(Kodok_awal*2)
	ldi XH, high(KODOK_ARR)
	ldi XL, low(KODOK_ARR)
	lpm temp, Z+
	ldi temp2, 0 
	mov s0, temp2 ;-- counter index

	; baca dari flash sampai ketemu 00. pindahkan ke SRAM
	loop:

		; -- tulis nilai flash ke RAM
		st X+, temp

 		; -- tulis di layar nilai sekarang
 		mov arg1, temp
 		mov arg2, s0
 		rcall WRITE_TEXT

		; -- update pointers
		lpm temp, Z+

		ldi temp2, -1
		sub s0, temp2
		TST temp
		brne loop

	ldi temp, 3
	mov empty_rock, temp
	ret

; --- BAGIAN INISIALISASI SELESAI ---

; --- BAGIAN HELPER ---

; --- delay
DELAY_01:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; DELAY_CONTROL 40 000 cycles
	; 5ms at 8.0 MHz
 		
	    ldi  r18, 52
	    ldi  r19, 242
	L1: dec  r19
	    brne L1
	    dec  r18
	    brne L1
	    nop
	ret

; -- button interrupt! reset permainan
ext_int2:
	rcall CLEAR_LCD
	ldi ZL, low(String_reset*2)
	ldi ZH, high(String_reset*2)

	rcall WRITE_WORD
	rcall DELAY_01
	rcall INIT_GAME ; -- inisialisasi ulang stack

	; -- mau ulang dari readkey

	; -- init ulang stack pointer
	ldi temp, low(RAMEND)
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp

	; -- simpan address readkey ke spl, lalu reti akan kesana
	; -- sneak 100
	ldi temp, low(read_key)
	push temp
	ldi temp, high(read_key)
	push temp
	reti

ov_int:
	
	; -- kalau dah 0 berarti gapenting, belum setting
	TST seconds_passed
	brne continue_interrupt
		reti

	continue_interrupt:
	; -- simpen nilai sebelum keluar
	push temp
	push temp2
	subi seconds_passed, 1
	TST seconds_passed
	brne continue_ov ; engga nol? lanjut

		; sudah nol, update LED
		; nyalain next led
		or led_lights, next_led
		out PORTC, led_lights
		
		; update values
		ldi seconds_passed, twenty_seconds
		lsl next_led

		; cek semua nyala?
		ldi temp, 0x40
		cp temp, next_led
		brne continue_ov

			; -- init ulang stack pointer
			ldi temp, low(RAMEND)
			out SPL, temp
			ldi temp, high(RAMEND)
			out SPH, temp

			ldi temp, low(LOSE_GAME)
			push temp
			ldi temp, high(LOSE_GAME)
			push temp
			reti

	continue_ov:

	; -- balikin temp dan temp2 barangkali penting
	
	
	pop temp2
	pop temp
	reti

; urutannya bawah - atas, kiri - kanan
KeyTable:
.DB 0xFF,0xFF,0xFF,0xFF ; fourth column, urutannya kiri-kanan
.DB 0xFF,0xFF,0xFF,0xFF; third column, keys #, 9, 6 und 3
.DB 0x04,0x05,0x06,0xFF; second column, keys 0, 8, 5 und 2
.DB 0x00,0x01,0x02,0x03; First column, keys *, 7, 4 und 1

Kodok_awal:
 .DB 0x41, 0x41, 0x41, 0x5F, 0x42, 0x42, 0x42, 0x00

Kodok_akhir:
.DB 0x42, 0x42, 0x42, 0x5F, 0x41, 0x41, 0x41, 0x00

String_menang: ; "SUCCESS"
.DB 0x53, 0x55, 0x43, 0x43, 0x45, 0x53, 0x53, 0x00

String_kalah: ; "TIMESUP"
.DB 0x54, 0x49, 0x4D, 0x45, 0x53, 0x55, 0x50, 0x00

String_reset: ; "RESET"
.DB 0x20, 0x52, 0x45, 0x53, 0x45, 0x54, 0x00, 0x00
