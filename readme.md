A. Cara jalanin:

1. Buka AVR studio, set lompat_kodok.asm sebagai entry file
2. Buka hapsim. load konfigurasi
3. build & run di avr studi
4. debug -> run di avr studia

B. Cara memainkan:

Tombol pada keypad diasosiasikan dengan sebuah posisi. Tombol 1 untuk posisi 1, tombol 2 untuk posisi 2, dan seterusnya.
Tekan tombol i untuk menggerakkan kodok pada posisi i:
- jika kodok dapat gerak, penggerakkan akan dilakukan secara automatis ke tujuan.
- Jika tidak, LED ke-7 akan berkedip

C. Reset button

Jika ingin mengulang permainan, tekan tombol "Reset Game". Permainan akan diulang dari awal. Timer, posisi kodok akan direset. Berlaku juga jika permainan sudah selesai

D. Timer:

Setiap 20 detik, satu buah LED akan menyala. Jika ke-enam LED menyala, waktu habis dan anda kalah. Anda tidak dapat bergerak lagi jika sudah kalah. Kecuali menekan tombol reset.

E. Syarat menang:

Anda menang jika kodok mencapai posisi B B B _ A A A. Anda tidak dapat bergerak lagi. Jika ingin mengulang, tekan tombol "Reset Game"

F. Video:
link: https://youtu.be/tAq5Jmn0fbE